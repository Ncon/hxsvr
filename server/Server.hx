package hxsvr.server;

#if cpp
import cpp.net.ThreadServer;
/*#elseif neko
import neko.net.ThreadServer;
#end*/

import hxsvr.server.comm.cnx.SocketConnection;
import hxsvr.shared.comm.cnx.IConnection;
import sys.net.Host;

// for ws
/*import haxe.crypto.Base64;
import haxe.crypto.Sha1;
import haxe.io.Bytes;*/

typedef Client = { var id : Int; };
typedef ClientMessage = { var data : String; };

/**
 * Server class
 * Handles connection and communcation with clients
**/
class Server extends ThreadServer<Client, ClientMessage>
{
	private var m_clientCount:Int;
	private var m_connectedClients:Map<Int, IConnection>;

	/**
	 * Creates a new Server object
	 * Make sure to run() it!
	**/
	public function new()
	{
		m_clientCount = 0;
		m_connectedClients = new Map<Int, IConnection>();
		super();
	}

	/*
	 * Overrides of base class functions
	 */

	/**
	 * Event callback invoked when a client is added
	**/
	public override function clientConnected( s:sys.net.Socket ) : Client
	{
		var id = addConnection( new SocketConnection( s ) );
    	return { id: id };
	}

	/**
	 * Adds a new connection to the connectedClients map
	 *
	 * @param cnx The connection to be added
	 *
	 * @return The ID of the connected client
	**/
	public function addConnection( cnx:IConnection ) : Int
	{
		m_connectedClients.set( m_clientCount, cnx );
		onClientConnection( m_clientCount );

		return m_clientCount++; // increment after returning
	}

	/**
	 * Event callback invoked when a client disconnects
	**/
	public override function clientDisconnected( c:Client )
	{
		onClientDisconnect( c.id );

		// Safety check
		if( m_connectedClients.exists( c.id ) )
		{
			m_connectedClients.remove( c.id );
		}
	}

	/**
	 * Event callback invoked when a message is recieved from a client
	**/
	public override function readClientMessage( c:Client, buf:haxe.io.Bytes, pos:Int, len:Int )
	{
		var data:String = buf.toString();

		if( StringTools.fastCodeAt( data, 0 ) == '{'.code )
		{
			return { msg: { data: data }, bytes: len };
		}
		else if( StringTools.startsWith( data, "<policy-file-request/>" ) )
		{
			// TODO: Move flash handling to own function/class
			trace( "Handling policy file request for client " + c.id );
			var pol = '<cross-domain-policy><allow-access-from domain="*" to-ports="*" /></cross-domain-policy>\x00';
			m_connectedClients[ c.id ].send( pol );
			return null;
		}
		/*else if( StringTools.startsWith( data, "GET" ) ) 
		{
			// TODO: Move WS handling to own function/class
			trace( "Handling Websocket Handshake for client " + c.id );

			// Impl borrowed from haxe-ws
			// https://github.com/soywiz/haxe-ws

			var requestLines = data.split( '\r\n' );

			var regexp = ~/^(.*): (.*)$/;
			var key = "";

			for( header in requestLines )
			{
				if( !regexp.match( header ) )
				{
					continue;
				}

				var name = regexp.matched( 1 );
				var value = regexp.matched( 2 );
				switch( name )
				{
					case 'Sec-WebSocket-Key': key = value;
					//case 'Sec-WebSocket-Version': version = value;
					//case 'Upgrade': upgrade = value;
					//case 'Connection': connection = value;
				}
			}

			var acceptKey = Base64.encode( Sha1.make( Bytes.ofString( key + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11' ) ) );

			var response : String = [
				'HTTP/1.1 101 Switching Protocols',
				'Upgrade: websocket',
				'Connection: Upgrade',
				'Sec-WebSocket-Accept: $acceptKey',
				'',	''
			].join('\r\n');

			m_connectedClients[ c.id ].send( response );

			return null;
		}*/
		else
		{
			return null;
		}
	}

	/**
	 * Event callback invoked after a message is read by readClientMessage
	**/
	public override function clientMessage( c:Client, msg:ClientMessage ) : Void
	{
		onData( msg.data );
	}

	/**
	 * Starts the server
	 *
	 * @param host Host IP
	 * @param port Host port
	**/
	public override function run( host:String, port:Int )
	{
		#if standalone
		trace( 'Initializing server at $host:$port' );
		trace( "Local IP: " + getIP() );
		#end

		// Borrowed from ThreadServer.run
		// but we don't want that while loop
		sock = new sys.net.Socket();
		sock.bind( new sys.net.Host( host ), port );
		sock.listen( listen );
		init();

		cpp.vm.Thread.create( checkForConnection );
	}

	/**
	 * Checks for new client connections
	**/
	public function checkForConnection()
	{
		// From ThreadServer.run
		try
		{
			addSocket( sock.accept() );
		}
		catch( e : Dynamic )
		{
				logError( e );
		}
	}

	/**
	 * Closes the server
	**/
	public function close()
	{
		#if standalone
		trace( "Closing Server..." );
		#end
		sock.close();
	}

	/**
	 * Broadcasts a message to all connected clients
	 *
	 * @param jsonData The message to send as a JSON string
	**/
	public function broadcast( jsonData:String )
	{
		for( connection in m_connectedClients )
		{
			connection.send( jsonData );
		}
	}

	/*
	 * Accessors to get server's clients
	 */

	/**
	 * Gets the connected clients map
	**/
	public function getConnections()
	{
		return m_connectedClients;
	}

	/**
	 * Gets a particular connected client by ID
	 * 
	 * @param clientID ID of the client to get
	 *
	 * @return The connectection, if it exists. Returns null if not found
	**/
	public function getConnection( clientID:Int )
	{
		if( m_connectedClients.exists( clientID ) )
		{
			return m_connectedClients[ clientID ];
		}
		else
		{
			return null;
		}
	}

	/*
	 * Rebindable functions to recieve connect/disconnect/data events
	 */
	
	public dynamic function onClientConnection( clientID:Int ) : Void
	{
		trace( 'New client connected (ID: $clientID)' );
	}

	public dynamic function onClientDisconnect( clientID:Int ) : Void
	{
		trace( 'Client $clientID disconnected' );
	}

	public dynamic function onData( data:String ) : Void
	{
		trace( "Recieved data: " + data );
	}

	/*
	 * Static util functions 
	 */
	
	/**
	 * Gets the local IP as an IPv4 string
	**/
	public static function getLocalIP() : String
	{
		var host = new sys.net.Host( Host.localhost() );
		
		var ip = host.ip;
		trace( ip );
		var p0 = ip & 0xFF;
		var p1 = (ip >> 8) & 0xFF;
		var p2 = (ip >> 16) & 0xFF;
		var p3 = (ip >> 24) & 0xFF;

		return '$p0.$p1.$p2.$p3';
	}
}
#end
