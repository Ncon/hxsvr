package hxsvr.server.comm.cnx;

import hxsvr.shared.comm.cnx.IConnection;

import sys.net.Socket;

/**
 * Server-side SocketConnection class
 * Wrapper for server connection to a sys.net Socket
**/
class SocketConnection implements IConnection
{
	/**
	 * sys Socket handled by connection
	**/
	private var m_socket : Socket;

	/**
	 * Creates a new SocketConnection object
	 *
	 * @param socket The sys Socket this connection handles
	**/
	public function new( socket:Socket )
	{
		m_socket = socket;
	}

	/**
	 * Writes data to the socket
	 *
	 * @param data String of data to send
	**/
	public function send( data:String )
	{
		m_socket.write( data );
	}

	/**
	 * Closes the Socket
	**/
	public function close()
	{
		m_socket.close();
	}
}
