package hxsvr.server;

/**
 * Server-side App interface
 *
 * Declares functionality for app run by server
**/
@:allow( hxsvr.server.HostedApp )
interface IServerApp
{
	private var serverRef( default, default ) : Server;

	public function init() : Void;
	public function tick() : Void;
	public function shutdown() : Void;
}
