package hxsvr.server;

/**
 * 'HostedApp' is a container class for running a 
 * server and server-side app concurrently
 *
 * [Pending changes]
**/
class HostedApp
{
	private var m_appInst : IServerApp;
	private var m_server : Server;

	public function new( app:IServerApp )
	{
		m_server = new Server();
		m_appInst = app;
		m_appInst.serverRef = m_server;
	}

	public function startServer( host:String, port:Int )
	{
		m_appInst.init();
		m_server.run( host, port );
	}

	public function closeServer()
	{
		m_server.close();
		m_appInst.shutdown();
	}

	public function update()
	{
		//m_server.checkForConnection();
		m_appInst.tick();
	}

	public function getAppInstance() : IServerApp
	{
		return m_appInst;
	}

	public function getServer() : Server
	{
		return m_server;
	}
}
