package hxsvr.client;

import hxsvr.client.comm.cnx.SocketConnection;

import js.Browser;
import js.html.ButtonElement;
import js.html.InputElement;

// Testing class for websockets
class JsClient
{
	private var m_cnx : SocketConnection;

	public function new()
	{
		var btn = cast( Browser.document.getElementById( "connectBtn" ), ButtonElement );
		btn.onclick = onButtonClick;
	}

	private function onButtonClick()
	{
		var textbox = cast( Browser.document.getElementById( "ipTxtBox" ), InputElement );
		m_cnx = new SocketConnection( textbox.value, 5000 );
		trace( textbox.value );

		Browser.document.body.innerHTML = "";

		m_cnx.getSocket();
	}

	public static function main()
	{
		var client = new JsClient();
	}
}
