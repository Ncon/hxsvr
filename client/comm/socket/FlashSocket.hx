package hxsvr.client.comm.socket;

import flash.events.ProgressEvent;
import flash.events.IOErrorEvent;
import flash.events.Event;
import flash.net.Socket;
//import flash.utils.ByteArray;

/**
 * 'FlashSocket' implementation
 * Handles socket communication for Flash platform
**/
class FlashSocket implements ISocket
{
	private var m_socketImpl : Socket;

	public function new()
	{
		m_socketImpl = new Socket();
		setupEventListeners();
	}

	/*
	 * Interface function definitions
	 */
	
	/**
	 * Attempts to connect to the given host
	**/
	public function connect( host:String, port:Int )
	{
		m_socketImpl.connect( host, port );
	}

	/**
	 * Sends data to the server
	**/
	public function send( data:String )
	{
		m_socketImpl.writeMultiByte( data, "UTF-8" );
		m_socketImpl.flush();
	}

	/**
	 * Closes the socket, disconnecting from the server
	**/
	public function close()
	{
		m_socketImpl.close();
	}

	/*
	 * Rebindable interface functions
	 */
	
	/**
	 * Rebindable callback invoked when the connection is established
	**/
	public dynamic function onConnect()
	{
		trace( "Connected!" );
	}

	/**
	 * Rebindable callback invoked when the connection is closed
	**/
	public dynamic function onClose()
	{
		trace( "Disconnected!" );
	}

	/**
	 * Rebindable callback invoked when data is recieved
	**/
	public dynamic function onData( data:String )
	{
		trace( "Data: " + data );
	}

	/**
	 * Rebindable callback invoked when an error occurs
	**/
	public dynamic function onError( e:Dynamic )
	{
		trace( "FlashSocket Error: " + e );
	}

	/*
	 * Accessors and util functions
	 */
	
	public function isConnected()
	{
		return m_socketImpl.connected;
	}

	/**
	 * Assigns callbacks the socket
	**/
	private function setupEventListeners()
	{
		m_socketImpl.addEventListener( Event.CONNECT, function( e:Event ) {
			onConnect();
        } );
		
        m_socketImpl.addEventListener( Event.CLOSE, function( e:Event ) {
			onClose();
        } );

        m_socketImpl.addEventListener( IOErrorEvent.IO_ERROR, function( e:IOErrorEvent ) {
			onError( e );
        } );

        m_socketImpl.addEventListener( ProgressEvent.SOCKET_DATA, function( e:ProgressEvent ) {			
			var data = m_socketImpl.readMultiByte( m_socketImpl.bytesAvailable, "UTF-8" );
			onData( data );
        } );
	}
}