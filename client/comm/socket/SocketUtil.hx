package hxsvr.client.comm.socket;

/**
 * Utility class for cross-platform socket usage
**/
class SocketUtil
{
	/**
	 * Creates the approriate socket for the client platform
	**/
	public static function createSocket() : ISocket
	{
		#if flash
		return new FlashSocket();
		#elseif js
		//#error "Websockets aren't working atm :("
		return new JsSocket();
		#elseif sys
		return new SysSocket();
		#else
		#error "Unsupported platform"
		#end
	}
}
