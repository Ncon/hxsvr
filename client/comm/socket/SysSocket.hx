package hxsvr.client.comm.socket;

import sys.net.Host;
import sys.net.Socket;

/**
 * 'SysSocket' implementation
 *
 * Handles socket communication for sys platforms
 * (cpp, cs, hl, java, lua, macro, neko, php, python)
**/
class SysSocket implements ISocket
{
	private var m_socketImpl : Socket;
	private var m_readTimer : haxe.Timer;

	private var m_connected : Bool;

	public function new()
	{
		m_socketImpl = new Socket();
		m_readTimer = new haxe.Timer( 100 ); //TODO: Make configurable

		m_connected = false;
	}

	/*
	 * Interface function definitions
	 */
	
	/**
	 * Attempts to connect to the given host
	**/
	public function connect( host:String, port:Int )
	{
		 try
		 {
            m_socketImpl.connect( new Host( host ), port );
            m_socketImpl.setBlocking( false );
			onConnect();
			m_connected = true;
			m_readTimer.run = readData;
        }
		catch( e:Dynamic )
		{
            onError( e );
			m_socketImpl.close();
        }
	}

	/**
	 * Sends data to the server
	**/
	public function send( data:String )
	{
		m_socketImpl.write( data );
	}

	/**
	 * Closes the socket, disconnecting from the server
	 * (Also stops the read timer)
	**/
	public function close()
	{
		m_readTimer.stop();
		onClose();
		m_socketImpl.close();
	}

	/*
	 * Rebindable interface functions
	 */
	
	/**
	 * Rebindable callback invoked when the connection is established
	**/
	public dynamic function onConnect()
	{
		trace( "Connected!" );
	}

	/**
	 * Rebindable callback invoked when the connection is closed
	**/
	public dynamic function onClose()
	{
		trace( "Disconnected!" );
	}

	/**
	 * Rebindable callback invoked when data is recieved
	**/
	public dynamic function onData( data:String )
	{
		trace( "Data: " + data );
	}

	/**
	 * Rebindable callback invoked when an error occurs
	**/
	public dynamic function onError( e:Dynamic )
	{
		trace( "SysSocket Error: " + e );
	}

	/*
	 * Accessors and util functions
	 */
	
	public function isConnected()
	{
		return m_connected;
	}

	/**
	 * Periodically reads input buffer for data from server
	**/
	private function readData()
	{
		var result = null;
		try
		{
			result = sys.net.Socket.select( [ m_socketImpl ], [], [], 0 );
		}
		catch( e:Dynamic )
		{
			onError( e );
		}

		if( result != null && result.read.length <= 0 )
		{
			// Nothing to read, early return;
			return;
		}

		var buf = new haxe.io.BytesBuffer();
		try
		{
			while( true )
			{
				var data = haxe.io.Bytes.alloc( 256 );
				//var bytesRead = m_socketImpl.input.readBytes( data, 0, data.length );

				buf.add( data );
			}
		}
		catch( e:haxe.io.Eof )
		{
			// Data no longer available (server closed!)
			//onError( e );
			close();
			return;
		}
		catch( e:Dynamic )
		{
			if( e != haxe.io.Error.Blocked )
			{
				onError( e );
				return;
			}
		}

		onData( buf.getBytes().toString() );
	}
}
