package hxsvr.client.comm.socket;

import js.html.MessageEvent;
import js.html.WebSocket;

// TODO: This
class JsSocket implements ISocket
{
	private var m_socketImpl : WebSocket;
	private var m_connected : Bool;

	public function new()
	{
		m_connected = false;
		// Socket connects on creation soooo...
	}

	public function connect( host:String, port:Int )
	{
		m_socketImpl = new WebSocket( "ws://" + host + ":" + port + "/" );
		setupEventListeners();
	}

	public function send( data:String )
	{
		m_socketImpl.send( data );
	}

	public function close()
	{
		m_socketImpl.close();
	}

	public dynamic function onConnect()
	{
		trace( "Open" );
	}

	public dynamic function onClose()
	{
		trace( "Close" );
	}

	public dynamic function onData( data:String )
	{
		trace( "Data: " + data );
	}
	
	public dynamic function onError( e:Dynamic )
	{
		trace( "Error" );
	}

	private function setupEventListeners()
	{
		m_socketImpl.onopen = function( e:MessageEvent ) {
			m_connected = true;
			onConnect();
			trace( e );
		};

		m_socketImpl.onclose = function( e:MessageEvent ) {
			onClose();
			trace( e );
		};

		m_socketImpl.onmessage = function( e:MessageEvent ) {
			onData( "Message" );
			trace( e );
		};

		m_socketImpl.onerror = function( e:MessageEvent ) {
			onError( null );
			trace( e );
		};
	}

	public function isConnected()
	{
		return m_connected;
	}
}