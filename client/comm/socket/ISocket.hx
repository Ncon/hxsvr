package hxsvr.client.comm.socket;

typedef ConnectionEventCallback = Void->Void;
typedef MessageEventCallback = String->Void;
typedef ErrorEventCallback = Dynamic->Void;

/**
 * Socket interface
 * Declares functionality for sockets of all client platforms 
**/
interface ISocket
{
	/*
	 * Socket control functions
	 */
	public function connect( host:String, port:Int ) : Void;
	public function send( data:String ) : Void;
	public function close() : Void;

	/*
	 * Rebindable event callback functions
	 */
	public dynamic function onConnect() : Void;
	public dynamic function onClose() : Void;
	public dynamic function onData( data:String ) : Void;
	public dynamic function onError( e:Dynamic ) : Void;

	/*
	 * Accessor functions
	 */
	public function isConnected() : Bool;
}
