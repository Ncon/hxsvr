package hxsvr.client.comm.cnx;

import hxsvr.shared.comm.cnx.IConnection;

import hxsvr.client.comm.socket.ISocket;
import hxsvr.client.comm.socket.SocketUtil;

/**
 * Client-side SocketConnection class
 *
 * Handles creating platform appropriate socket
 * and connecting/messaging the server
**/
class SocketConnection implements IConnection
{
	var m_socket : ISocket;

	/**
	 * Creates a new SocketConnection object
	 * and attempts to connect to host
	 *
	 * @param 	host 	The server IP
	 * @param 	port	The server Port
	 * @param 	connectCallback	Callback function to be invoked upon connection
	**/
	public function new( host:String, port:Int, connectCallback:ConnectionEventCallback )
	{
		onConnection = connectCallback;
		attemptConnection( host, port );
	}

	/**
	 * Sends data to the server
	 *
	 * @param data The message to send as a string
	**/
	public function send( data:String )
	{
		m_socket.send( data );
	}

	/**
	 * Closes the socket, disconnecting from server
	**/
	public function close()
	{
		m_socket.close();
	}

	/**
	 * Gets the socket
	 * Note: Returns ISocket, not specific impl
	**/
	public function getSocket() : ISocket
	{
		return m_socket;
	}

	/*
	 * Setters for socket event callbacks
	 * Note: Must be called after connection is established
	 * (i.e. in connectCallback)
	 */

	/**
	 * Sets event callback to be invoked when the socket is closed
	 * Note: Must be assigned after connection is established
	 *
	 * @param callback The callback function to be used
	**/
	public function setOnCloseCallback( callback:ConnectionEventCallback )
	{
		if( m_socket.isConnected() )
		{
			m_socket.onClose = callback;
		}
		else
		{
			trace( "WARNING: Socket event callbacks must be assigned after connection!" );
		}
	}

	/**
	 * Sets event callback to be invoked when data is recieved
	 * Note: Must be assigned after connection is established
	 *
	 * @param callback The callback function to be used
	**/
	public function setOnDataCallback( callback:MessageEventCallback )
	{
		if( m_socket.isConnected() )
		{
			m_socket.onData = callback;
		}
		else
		{
			trace( "WARNING: Socket event callbacks must be assigned after connection!" );
		}
	}

	/**
	 * Sets event callback to be invoked when there is an error
	 * Note: Must be assigned after connection is established
	 *
	 * @param callback The callback function to be used
	**/
	public function setOnErrorCallback( callback:ErrorEventCallback )
	{
		if( m_socket.isConnected() )
		{
			m_socket.onError = callback;
		}
		else
		{
			trace( "WARNING: Socket event callbacks must be assigned after connection!" );
		}
	}

	/*
	 * Connection handling funcs
	 */

	/**
	 * Attempt to establish a connection and verifies connection after delay
	**/
	private function attemptConnection( host:String, port:Int )
	{
		trace( "Attempting Connection" );
		m_socket = SocketUtil.createSocket();
		m_socket.onConnect = onConnection;
		m_socket.connect( host, port );

		// TODO: Configurable delay, timeout after x failed attempts
		haxe.Timer.delay( function() {
			checkConnection( host, port );
		}, 3000 );
	}

	/**
	 * Verifies the connection was established
	 * Attempts connection if it was not
	**/
	private function checkConnection( host:String, port:Int )
	{
		if( !m_socket.isConnected() )
		{
			trace( "Failed to connect..." ); // TODO: Remove trace/add debug conditional comp.
			//m_socket.close();
			attemptConnection( host, port );
		}
	}

	// Rebindable function assigned to m_socket.onConnect
	private dynamic function onConnection()
	{
		trace( "Connection Successful" );
	}
}
