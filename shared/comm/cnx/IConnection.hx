package hxsvr.shared.comm.cnx;

/**
 * Connection interface
 *
 * Declares functionality for Connection wrappers for client and server
 * Used to support lan, network, and local connections transparently
**/
interface IConnection
{
	public function send( data:String ) : Void;
	public function close() : Void;
}
